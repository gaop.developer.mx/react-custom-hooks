import { todoReducer } from "./todoReducer";
import {useReducer, useEffect } from "react"

const init = () => {
    return JSON.parse(localStorage.getItem('todos')) || [];
}

export const useTodo = () => {

    const [ todos, dispatch ] = useReducer( todoReducer, [], init );

    useEffect( () => {
        localStorage.setItem('todos', JSON.stringify(todos));
    }, [todos])
  
    const handleNewTodo = ( todo ) => {
        const action = {
            type: '[TODO] add todo',
            payload: todo
        }
        dispatch( action );
    }

    const handleDeleteTodo = (id) => {
        const action = {
            type: '[TODO] remove todo',
            payload: id
        }
        dispatch( action )
    }

    const handleToggleTodo = (id) => {
        const action = {
            type: '[TODO] toggle todo',
            payload: id
        }
        dispatch( action )
    }

    const todosCount = todos.length;
    const todosPendingCount = todos.filter( todo => !todo.done ).length;

    return {
    todos,
    todosCount,
    todosPendingCount,
    handleNewTodo,
    handleDeleteTodo,
    handleToggleTodo,
  }
}
